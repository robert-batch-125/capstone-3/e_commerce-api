
const User = require("./../models/User");
const Product = require('./../models/Products');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


//check email exists
module.exports.checkEmailExists = (reqBody) => {
	//console.log(reqBody)
	
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	})
}


//user registration
module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 


//logging in
module.exports.login = (reqBody) => { 
	//Model.method
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result === null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


//login details
module.exports.getProfile = (data) => {
	//console.log(data)
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}


//get all users
module.exports.getAllUsers = () => {
	
	return User.find().then( result => {
		return result
	})
}


//user set as admin
module.exports.userToAdmin = (userId) => {

	return User.findById(userId).then(user => {
			if(user === null){
				return false;
			}else{
				user.isAdmin = true;
				return user.save().then((updatedUser, error) => {
					if(error){
						return false;
					} else {
						return true;
					}
				})
			}
		})
	}


//get all admin
module.exports.getAllAdmin = () => {
	
	return User.find({isAdmin: true}).then( result => {		
		return result
	})
}


//check-out order
module.exports.checkoutOrder = async (data) => {

	//save user checkouts
		const userSaveStatus = await User.findById(data.userId).then( user => {
			// console.log(user)
			user.checkouts.push({productId: data.productId})

			return user.save().then( (user, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		})

		//save product orders
		const productSaveStatus = await Product.findById(data.productId).then( product => {
			product.orders.push({userId: data.userId})

			return product.save().then( (product, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		})


		if(userSaveStatus && productSaveStatus){
			return true
		} else {
			return false
		}
	}



//retrieve all orders(admin only)
module.exports.retrieveAllOrders = () => {
	
	return User.find({isAdmin: false}).then(users => {

		let allOrders = [];

		users.forEach(user => {

			allOrders.push({
				userId: user._id,
				lastName: user.lastName,
				email: user.email,				
				checkouts: user.checkouts
			});
		})
		return allOrders;
	})
}


//retrieve user order details
module.exports.retrieveMyOrders = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = undefined;
		return result;
	})
}

