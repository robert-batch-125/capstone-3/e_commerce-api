const express = require('express');
const router = express.Router();
let auth = require('./../auth');
//controllers
const productController = require('./../controllers/productControllers');


//add product
router.post('/addProduct', auth.verify, (req, res) => {
	//console.log(req.body)
	
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.addProduct(req.body).then(result => res.send(result))
	}
})


//retrieve all active products
router.get('/active', (req, res) => {

	productController.allActiveProducts().then( result => res.send(result))
})


//retrieve all products
router.get("/all", auth.verify, (req, res) => {

	productController.getAllProducts().then( result => res.send(result))
})


//retrieve single product
router.get('/:productId', (req, res) => {

	productController.singleProduct(req.params.productId).then( result => res.send(result))
})


//retrieve all products
router.get('/all', (req, res) => {

	productController.allProducts().then( result => res.send(result))
})


//update product
router.put('/:productId/edit', auth.verify, (req, res) => {
	//console.log(req.params.productId)

	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.updateProduct(req.params.productId, req.body).then( result => res.send(result))
	}
	
})


//archieve product
router.put('/:productId/archive', auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.archiveProduct(req.params.productId).then( result => res.send(result))
	}	
})


//unarchieve product
router.put('/:productId/unarchive', auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.unarchiveProduct(req.params.productId).then( result => res.send(result))
	}	
})


//delete a product
router.delete('/:productId/delete', auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.deleteProduct(req.params.productId).then( result => res.send(result))
	}	
})


module.exports = router;
